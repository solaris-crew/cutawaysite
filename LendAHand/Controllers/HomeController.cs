﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using LendAHand.Models;
using LendAHand.Services;

namespace LendAHand.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> logger;
        private readonly ApiProvider apiProvider;
        private const int TARGET = 1;

        public HomeController(ILogger<HomeController> logger, ApiProvider apiProvider)
        {
            this.logger = logger;
            this.apiProvider = apiProvider;
        }

        public async Task<IActionResult> Index(int lang = 1)
        {
            logger.LogInformation("Start render");
            var list = await apiProvider.GetAllResources(lang, TARGET);
            var dict = new Dictionary<string, string>();
            list.ForEach(delegate (ResourceDto res)
            {
                dict.Add(res.Code, res.Value);
            });
            logger.LogInformation("End render");
            return View(dict);
        }

        public async Task<IActionResult> Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public async Task<IActionResult> Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
