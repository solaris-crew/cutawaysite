﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendAHand.Models
{
    public class ResourceDto
    {
        public string Code { get; set; }
        public string Value { get; set; }
        public int LanguageId { get; set; }
        public int TargetId { get; set; }
    }
}
