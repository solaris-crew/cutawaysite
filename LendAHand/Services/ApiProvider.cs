﻿using LendAHand.Models;
using Microsoft.Extensions.Options;
using Refit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendAHand.Services
{
    public class ApiProvider
    {
        private readonly IApiProvider apiProvider;
        public ApiProvider(IOptions<ApiSettings> settings)
        {
            apiProvider = RestService.For<IApiProvider>(settings.Value.UrlAddress);
        }

        public async Task<List<ResourceDto>> GetAllResources(int languageId, int targetId)
        {
            return await apiProvider.GetAllResources(languageId, targetId);
        }
    }
}
