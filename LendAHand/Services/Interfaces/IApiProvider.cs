﻿using LendAHand.Models;
using Refit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendAHand.Services
{
    public interface IApiProvider
    {
        [Get("/api/resources/all/{languageId}/{targetId}")]
        Task<List<ResourceDto>> GetAllResources(int languageId, int targetId);
    }
}
