﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendAHand.Services.Interfaces
{
    public interface IApiSettings
    {
        public string UrlAddress { get; set; }
    }
}
