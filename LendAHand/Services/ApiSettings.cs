﻿using LendAHand.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendAHand.Services 
{
    public class ApiSettings : IApiSettings
    {
        public string UrlAddress { get; set; }
    }
}
